﻿<?php
// Отправляем правильную кодировку.
header('Content-Type: text/html; charset=UTF-8');
print('Привет, мир!');
// Выводим все полученные через POST параметры.
// если запрос 2-5 сделан правильно, то можно будет увидеть
// отправленный комментарий в ответе веб-сервера.
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
 print_r($_POST);
}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    
  </head>
  <body>
   <p> 1) получить главную страницу методом GET в протоколе HTTP 1.0;
 </p>
<p> <img src="z1.png"> </p>

 <p> 2) получить внутреннюю страницу методом GET в протоколе HTTP 1.1; </p>
<p> <img src="z2.png"> </p>

 <p> 3) определить размер файла file.tar.gz, не скачивая его;</p>
<p> <img src="z3.png"> </p>

<p> 4) определить медиатип ресурса /image.png;</p>
<p> <img src="z4.png"> </p>

<p> 5) отправить комментарий на сервер по адресу /index.php;
</p>
<p> <img src="z5.png"> </p>

<p>6) получить первые 100 байт файла /file.tar.gz;</p>
<p> <img src="z6.png"> </p>

<p> 7) определить кодировку ресурса /index.php.
</p>
<p> <img src="z7.png"> </p>

  </body>
</html>
